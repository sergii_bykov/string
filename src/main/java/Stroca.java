import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class Stroca {
    public static void main(String[] args) {


// 1.Вывести в одну строку символы: английского алфавита от ‘A’ до ‘Z’

        for (char i = 'A'; i <= 'Z'; i++) {
            System.out.print(i + " ");
        }
        System.out.println();

        //Вывести в одну строку символы: английского алфавита от ‘z’ до ‘a’

        for (char i = 'z'; i >= 'a'; i--) {
            System.out.print(i + " ");
        }
        System.out.println();

        //Вывести в одну строку символы: английского алфавита от ‘а’ до ‘я’

        for (char i = 'а'; i <= 'я'; i++) {
            System.out.print(i + " ");
        }
        System.out.println();

        //Вывести в одну строку символы: английского алфавита от ‘0’ до ‘9’

        for (char i = '0'; i <= '9'; i++) {
            System.out.print(i + " ");
        }

        //Вывести в одну строку символы: печатного диапазона таблицы ASCII
        System.out.println();
        for (int i = 65; i < 90; i++) {
            char ch = (char) i;
            System.out.print(ch + " ");
        }
        for (int i = 97; i < 122; i++) {
            char ch = (char) i;
            System.out.print(ch + " ");
        }
        //  2.Написать и протестировать функции преобразования: вещественного числа в строку
        System.out.println();
        double num = 32.4e10;
        String str1 = Double.toString(num);
        System.out.println(str1);

        //  2.Написать и протестировать функции преобразования: целого  числа в строку
        int number = 5;
        String str = Integer.toString(number);
        System.out.println(str);

        //  2.Написать и протестировать функции преобразования: целого  числа в строку
        String myString = "5";
        int i = Integer.parseInt(myString);
        System.out.println(i);

        //  2.Написать и протестировать функции преобразования: целого  числа в строку
        String myString1 = "5.45";
        double k = Double.parseDouble(myString1);
        System.out.println(k);


        //3.Дана строка, состоящая из слов, разделенных пробелами и знаками препинания. Определить длину самого короткого слова.

        String strInput1 = "У Лукоморья дуб зеленый, златая цепь на дубе том" +
                " и днём и ночью кот учёный, всё ходит по цепи кругом";
        String strInput = strInput1.trim();
        String[] splitArray = strInput.split("([^а-яА-Яa-zA-Z']+)'*\\1*");
        int minLength = Integer.MAX_VALUE;
        for (String word : splitArray) {
            int wordLength = word.length();
            if (wordLength < minLength) {
                minLength = wordLength;
            }
        }
        System.out.println("min = " + minLength);

        //Дан массив слов. Заменить последние три символа слов, имеющих заданную длину на символ "$"
        String[] catsNames = {
                "Васька",
                "Кузя",
                "Барсик",
                "Мурзик",
                "Леопольд",
                "Бегемот",
                "Рыжик",
                "Матроскин"
        };
        // заданная длина - z
        int z = 6;

        for (int i1 = 0; i1 < catsNames.length; i1++) {
            String catsName = catsNames[i1];
            if (catsName.length() == z) {
                String s = catsName.substring(0, catsName.length() - 3) + "$";
                catsNames[i1] = s;
            }
        }
        System.out.println(Arrays.toString(catsNames));
        //Добавить в строку пробелы после знаков препинания, если они там отсутствуют.
        String s = "Lukomorye,has a green oak.A golden chain!on a tom oak-and day:and night the cat is a scientist, everything goes around-in chains";
        s = s.replaceAll("(?<=\\p{Punct})(?=\\w)", " ");
        System.out.println(s);

        //Оставить в строке только один экземпляр каждого встречающегося символа.

        String string = "Lukomorye,has a green oak.A golden chain!on a tom oak-and day:and night the cat is a scientist, everything goes around-in chains";
        char[] chars = string.toCharArray();
        Set<Character> charSet = new LinkedHashSet<Character>();
        for (char c : chars) {
            charSet.add(c);
        }

        StringBuilder sb = new StringBuilder();
        for (Character character : charSet) {
            sb.append(character);
        }
        System.out.println(sb.toString());

        //Подсчитать количество слов во введенной пользователем строке.

        Scanner sc = new Scanner(System.in);
        //Вводим предложение в консоль
        System.out.println("Введите слова одной строкой через пробел");
        String input = sc.nextLine();
        //Начальное количество слов равно 0
        int count = 0;

        //Если ввели хотя бы одно слово, тогда считать, иначе конец программы
        if (input.length() != 0) {
            count++;
            //Проверяем каждый символ, не пробел ли это
            for (int i2 = 0; i2 < input.length(); i2++) {
                if (input.charAt(i2) == ' ') {
                    //Если пробел - увеличиваем количество слов на 1
                    count++;
                }
            }
        }

        System.out.println("Вы ввели " + count + "слов");

        // Удалить из строки ее часть с заданной позиции и заданной длины.
        String strInput2 = "У Лукоморья дуб зеленый, златая цепь на дубе том" +
                " и днём и ночью кот учёный, всё ходит по цепи кругом";
        //задаем начало и конец.
        System.out.println(strInput2.substring(12, 67));

        //Перевернуть строку, т.е. последние символы должны стать первыми, а первые последними.
        String strInput3 = "У Лукоморья дуб зеленый, златая цепь на дубе том" +
                " и днём и ночью кот учёный, всё ходит по цепи кругом";
        System.out.println(new StringBuilder(strInput3).reverse());

        //В заданной строке удалить последнее слово
        String strInput4 = "У Лукоморья дуб зеленый, златая цепь на дубе том" +
                " и днём и ночью кот учёный, всё ходит по цепи кругом";
        System.out.println(strInput4.replaceAll(" [^ ]+$", ""));


    }
}




